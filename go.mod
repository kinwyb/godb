module gitee.com/kinwyb/godb

go 1.18

require (
	gitee.com/kinwyb/conv v0.0.0-20211105064504-a0a4178b4406
	github.com/denisenkom/go-mssqldb v0.12.3
	github.com/go-sql-driver/mysql v1.7.1
	github.com/golang-sql/sqlexp v0.1.0
	github.com/sirupsen/logrus v1.9.3
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/cast v1.6.0
	github.com/xwb1989/sqlparser v0.0.0-20180606152119-120387863bf2
)

require (
	github.com/golang-sql/civil v0.0.0-20220223132316-b832511892a9 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181017120253-0766667cb4d1 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
)
