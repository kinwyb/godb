package godb

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/spf13/cast"
	"net/url"
	"regexp"
	"strings"
	"time"
)

// EmptyUniqueidentifier 空的uniqueidentifier值
const EmptyUniqueidentifier = "00000000-0000-0000-0000-000000000000"

// UniqueidentifierValue 创建一个uniqueidentifier值，当可能是null的时候用
func UniqueidentifierValue(val string) sql.NullString {
	ret := sql.NullString{String: val}
	ret.Valid = ret.String != ""
	return ret
}

// MSSQL 操作对象
type MSSQL struct {
	conn
	mssqlQuery
	linkString string
}

func (m *MSSQL) Transaction(t TransactionFunc, option ...*sql.TxOptions) error {
	mtx := &mssqlTx{}
	mtx.SetDataBaseName(m.dbname)
	return m.newTransaction(t, mtx.GetTxSQL, option...)
}

func (m *MSSQL) Close() {
	m.conn.Close()
	m.mssqlQuery.exec = nil
}

// MSSQLConnect 链接mssql数据库
//
//eg:sqlserver://sa:mypass@localhost?database=master
func MSSQLConnect(host, username, password, db string, params ...url.Values) (*MSSQL, error) {
	query := url.Values{}
	if len(params) > 0 {
		query = params[0]
	}
	query.Set("database", db)
	u := &url.URL{
		Scheme: "sqlserver",
		User:   url.UserPassword(username, password),
		Host:   fmt.Sprintf("%s", host),
		// Path:  instance, // if connecting to an instance instead of a port
		RawQuery: query.Encode(),
	}
	result := &MSSQL{
		linkString: u.String(),
	}
	sqlDB, err := sql.Open("sqlserver", result.linkString)
	if err != nil {
		return nil, err
	}
	sqlDB.SetMaxOpenConns(30)                  //最大连接数
	sqlDB.SetMaxIdleConns(25)                  //最大的空闲连接数25个
	sqlDB.SetConnMaxIdleTime(10 * time.Minute) //空闲10分钟后释放连接
	result.SetSQLDB(sqlDB)
	result.SetDataBaseName(db) //记录数据库名称,表名格式化会用到
	result.exec = sqlDB
	result.driverName = MSSQLDriver
	return result, nil
}

type mssqlQuery struct {
	query
}

func (m *mssqlQuery) QueryRows(sql string, args ...interface{}) *QueryResult {
	i := 0
	sql = regexp.MustCompile("(\\?)").ReplaceAllStringFunc(sql, func(s string) string {
		i++
		return fmt.Sprintf("@p%d", i)
	})
	if len(args) < i {
		return ErrQueryResult(fmt.Errorf("参数缺少,目标参数%d个,实际参数%d个", i, len(args)), m.dbname, sql, args)
	}
	return m.query.QueryRows(sql, args...)
}

func (m *mssqlQuery) QueryRow(sql string, args ...interface{}) *QueryResult {
	if ok, _ := regexp.MatchString("(?i)(.*?) TOP (.*?)\\s+(.*)?", sql); ok {
		sql = regexp.MustCompile("(?i)(.*?) TOP (.*?)\\s+(.*)?").ReplaceAllString(sql, "SELECT TOP 1 $3")
	} else {
		sql = strings.Replace(sql, "SELECT ", "SELECT TOP 1 ", 1)
	}
	return m.QueryRows(sql, args...)
}

func (m *mssqlQuery) QueryWithPage(sql string, page *PageObj, args ...interface{}) *QueryResult {
	if page == nil {
		return m.QueryRows(sql, args...)
	}
	regx := regexp.MustCompile("\\s+(\\[?[a-zA-Z0-9]+\\]+\\.)?\\[?[a-zA-Z0-9]+\\]?\\.[a-zA-Z]*\\.+")
	tableRepMap := map[string]string{}
	if regx.MatchString(sql) {
		sql = regx.ReplaceAllStringFunc(sql, func(s string) string {
			news := strings.ReplaceAll(s, "[", "@")
			news = strings.ReplaceAll(news, "]", "@")
			news = strings.ReplaceAll(news, ".", "@")
			tableRepMap[strings.TrimSpace(news)] = strings.TrimSpace(s)
			return news
		})
	}
	sqlInfo, err := mysqlSelectSQLParser(sql)
	if err != nil {
		return ErrQueryResult(err, m.dbname, sql, args)
	}
	sqlInfo.table = strings.ReplaceAll(sqlInfo.table, "`", "")
	if len(tableRepMap) > 0 {
		for k, v := range tableRepMap {
			sqlInfo.table = strings.ReplaceAll(sqlInfo.table, k, v)
			sqlInfo.where = strings.ReplaceAll(sqlInfo.where, k, v)
		}
	}
	sqlBuilder := strings.Builder{}
	if page.Total < 1 {
		sqlBuilder.WriteString("SELECT count(0) num FROM ")
		sqlBuilder.WriteString(sqlInfo.table)
		sqlBuilder.WriteString(sqlInfo.where)
		sqlBuilder.WriteString(sqlInfo.groupBy)
		sqlBuilder.WriteString(sqlInfo.having)
		result := m.QueryRows(sqlBuilder.String(), args...)
		e := result.Error()
		if e != nil {
			return result
		}
		count := cast.ToInt64(cast.ToString(result.Get("num")))
		page.SetTotal(count)
		sqlBuilder.Reset()
	}
	currentpage := 0
	if page.Page-1 > 0 {
		currentpage = page.Page - 1
	}
	if page.Total < 1 {
		return NewRowsResult(nil, sql, args)
	}
	sqlBuilder.WriteString("SELECT TOP ")
	sqlBuilder.WriteString(cast.ToString(page.Rows))
	sqlBuilder.WriteString(" * FROM (SELECT ROW_NUMBER() OVER (")
	sqlBuilder.WriteString(sqlInfo.orderBy)
	sqlBuilder.WriteString(") as RowNumber,")
	sqlBuilder.WriteString(sqlInfo.selectColumns)
	sqlBuilder.WriteString(" FROM ")
	sqlBuilder.WriteString(sqlInfo.table)
	sqlBuilder.WriteString(sqlInfo.where)
	sqlBuilder.WriteString(sqlInfo.groupBy)
	sqlBuilder.WriteString(sqlInfo.having)
	sqlBuilder.WriteString(") as tmp WHERE RowNumber > ")
	sqlBuilder.WriteString(cast.ToString(page.Rows * currentpage))
	sqlBuilder.WriteString(" ORDER BY RowNumber ASC ")
	sql = sqlBuilder.String()
	return m.QueryRows(sql, args...)
}

func (m *mssqlQuery) Exec(sql string, args ...interface{}) *ExecResult {
	i := 0
	sql = regexp.MustCompile("(\\?)").ReplaceAllStringFunc(sql, func(s string) string {
		i++
		return fmt.Sprintf("@p%d", i)
	})
	if len(args) < i {
		return ErrExecResult(fmt.Errorf("参数缺少,目标参数%d个,实际参数%d个", i, len(args)), m.dbname, sql, args)
	}
	return m.query.Exec(sql, args...)
}

func (m *mssqlQuery) Prepare(query string) (*sql.Stmt, error) {
	i := 0
	query = regexp.MustCompile("(\\?)").ReplaceAllStringFunc(query, func(s string) string {
		i++
		return fmt.Sprintf("@p%d", i)
	})
	return m.query.Prepare(query)
}

type mssqlTx struct {
	mssqlQuery
	tx  *sql.Tx
	err error
}

func (m *mssqlTx) GetTxSQL(tx *sql.Tx) TxSQL {
	m.tx = tx
	m.checkConn = false
	m.exec = tx
	return m
}

func (m *mssqlTx) GetTx() *sql.Tx {
	return m.tx
}

// Err 返回执行错误
func (m *mssqlTx) Err() error {
	return m.err
}

func (m *mssqlTx) QueryRows(sql string, args ...interface{}) *QueryResult {
	res := m.mssqlQuery.QueryRows(sql, args...)
	m.err = res.Error()
	return res
}

func (m *mssqlTx) QueryRow(sql string, args ...interface{}) *QueryResult {
	res := m.mssqlQuery.QueryRow(sql, args...)
	m.err = res.Error()
	return res
}

func (m *mssqlTx) QueryWithPage(sql string, page *PageObj, args ...interface{}) *QueryResult {
	res := m.mssqlQuery.QueryWithPage(sql, page, args...)
	m.err = res.Error()
	return res
}

func (m *mssqlTx) Exec(sql string, args ...interface{}) *ExecResult {
	if m.err != nil { //前面查询已经出错，会导致mssql直接回滚事务，所以后续的exec执行不应提交否则后续exec不受事务管控
		return ErrExecResult(errors.New("前面执行已出错中断后续操作"), m.dbname, sql, args)
	}
	res := m.mssqlQuery.Exec(sql, args...)
	m.err = res.Error()
	return res
}
