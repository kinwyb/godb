package godb

import (
	"errors"
	"fmt"
	"github.com/spf13/cast"
	"testing"
)

func TestMYSQLConnect(t *testing.T) {
	conn, err := MYSQLConnect("192.168.9.11:3306", "crystal_dev",
		"123456", "crystal_dev")
	if err != nil {
		t.Fatal(err)
	}
	defer conn.Close()
	res := conn.QueryRow("SELECT * FROM aps_op_complate LIMIT 1")
	if res.Error() != nil {
		t.Fatal(res.Error())
	}
	mo := res.Get("order_code")
	if cast.ToString(mo) != "FT-042-42-4" {
		t.Fatal("查询错误1")
	}
	res = conn.QueryRow("SELECT order_code FROM aps_op_complate WHERE order_code = ? ",
		"FT-042-42-4")
	if res.Error() != nil {
		t.Fatal(res.Error())
	}
	mo = res.Get("order_code")
	if cast.ToString(mo) != "FT-042-42-4" {
		t.Fatal("查询错误2")
	}
	pg := &PageObj{}
	res = conn.QueryWithPage("SELECT * FROM aps_op_complate WHERE order_code = ? ORDER BY id", pg, "FT-042-42-4")
	if res.Error() != nil {
		t.Fatal(res.Error())
	}
	mo = res.Get("order_code")
	if cast.ToString(mo) != "FT-042-42-4" {
		t.Fatal("查询错误3")
	} else if pg.Total != 1 {
		t.Fatal("分页总数错误4")
	}
	pg.Total = 0
	res = conn.QueryWithPage("SELECT * FROM aps_kint_wt", pg)
	if res.Length() != 20 {
		t.Fatal("分页数据查询错误")
	}
	// 删除
	err = conn.Exec("DELETE FROM aps_op_complate WHERE order_code IN (?,?)", "T123456", "T1234567").Error(false)
	if err != nil {
		t.Fatal(err)
	}
	var args = []interface{}{
		"T123456", "", "", "", 1, "2021-03-13 17:00:01", "0", 0,
	}
	// commit
	conn.Transaction(func(tx TxSQL) error {
		sql := "INSERT INTO aps_op_complate(order_code,style_code,size,color,tp,updateTime,user_id,flag) VALUES (?,?,?,?,?,?,?,?)"
		err := tx.Exec(sql, args...).Error(true)
		if err != nil {
			fmt.Println("insert 错误:", err.Error())
			return err
		}
		args[0] = "T1234567"
		err = tx.Exec(sql, args...).Error(true)
		if err != nil {
			fmt.Println("insert 错误:", err.Error())
			return err
		}
		return nil
	})
	// rollback
	conn.Transaction(func(tx TxSQL) error {
		args[0] = "T1234568"
		sql := "INSERT INTO aps_op_complate(order_code,style_code,size,color,tp,updateTime,user_id,flag) VALUES (?,?,?,?,?,?,?,?)"
		err := tx.Exec(sql, args...).Error(true)
		if err != nil {
			fmt.Println("insert 错误:", err.Error())
		}
		args[0] = "T1234569"
		err = tx.Exec(sql, args...).Error(true)
		if err != nil {
			fmt.Println("insert 错误:", err.Error())
		}
		return errors.New("rollback")
	})
	mo = conn.QueryRow("SELECT * FROM aps_op_complate WHERE order_code = ?", "T123456").Get("order_code")
	if cast.ToString(mo) != "T123456" {
		t.Fatal("insert 事务错误")
	}
	// 删除
	err = conn.Exec("DELETE FROM aps_op_complate WHERE order_code = ?", "T123456").Error(true)
	if err != nil {
		t.Fatal(err)
	}
	err = conn.Exec("DELETE FROM aps_op_complate WHERE order_code = ?", "T1234567").Error(true)
	if err != nil {
		t.Fatal(err)
	}
	// 删除
	err = conn.Exec("DELETE FROM aps_op_complate WHERE order_code = ?", "T1234568").Error(true)
	if err == nil {
		t.Fatal("空行删除无法检测")
	}
}
