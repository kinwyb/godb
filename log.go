package godb

import "github.com/sirupsen/logrus"

var log *logrus.Entry

// SetLogger 设置日志
func SetLogger(lg *logrus.Logger) {
	log = logrus.NewEntry(lg)
}

// SetLog 设置日志输出
func SetLog(lg *logrus.Entry) {
	log = lg
}
