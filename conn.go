package godb

import (
	"context"
	"database/sql"
	"time"
)

type DriverName string

const (
	MSSQLDriver DriverName = "mssql"
	MYSQLDriver DriverName = "mysql"
)

// sql.DB和sql.TxDB公共操作函数提取
type queryInf interface {
	QueryContext(ctx context.Context, sql string, args ...interface{}) (*sql.Rows, error)

	ExecContext(ctx context.Context, sql string, args ...interface{}) (sql.Result, error)

	Prepare(query string) (*sql.Stmt, error)
}

type query struct {
	exec         queryInf
	checkConn    bool
	dbname       string
	driverName   DriverName
	queryTimeOut time.Duration
}

func (q *query) connect() error {
	if q.exec == nil {
		return DatabaseNotOpen
	}
	if ping, ok := q.exec.(*sql.DB); ok {
		if err := ping.Ping(); err != nil {
			return ErrorPing
		}
	}
	return nil
}

func (q *query) QueryRowsWithContext(ctx context.Context, sql string, args ...interface{}) *QueryResult {
	if q.checkConn {
		if err := q.connect(); err != nil {
			return ErrQueryResult(err, q.dbname, sql, args)
		}
	}
	if q.queryTimeOut > 0 {
		newCtx, cancelFun := context.WithTimeout(ctx, q.queryTimeOut)
		ctx = newCtx
		defer cancelFun()
	}
	rows, err := q.exec.QueryContext(ctx, sql, args...)
	if err != nil {
		return ErrQueryResult(err, q.dbname, sql, args)
	}
	return NewRowsResult(rows, sql, args)
}

func (q *query) ExecWithContext(ctx context.Context, sql string, args ...interface{}) *ExecResult {
	if q.checkConn {
		if err := q.connect(); err != nil {
			return ErrExecResult(err, q.dbname, sql, args)
		}
	}
	if q.queryTimeOut > 0 {
		newCtx, cancelFun := context.WithTimeout(ctx, q.queryTimeOut)
		ctx = newCtx
		defer cancelFun()
	}
	result, err := q.exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return ErrExecResult(err, q.dbname, sql, args)
	}
	return NewExecResult(result, sql, args)
}

func (q *query) Prepare(query string) (*sql.Stmt, error) {
	if q.checkConn {
		if err := q.connect(); err != nil {
			return nil, err
		}
	}
	return q.exec.Prepare(query)
}

func (q *query) QueryRows(sql string, args ...interface{}) *QueryResult {
	return q.QueryRowsWithContext(context.Background(), sql, args...)
}

func (q *query) Exec(sql string, args ...interface{}) *ExecResult {
	return q.ExecWithContext(context.Background(), sql, args...)
}

// SetQueryTimeOut 设置查询超时时间
func (q *query) SetQueryTimeOut(outTime time.Duration) {
	q.queryTimeOut = outTime
}

// DataBaseName 数据库名称
func (q *query) DataBaseName() string {
	if q == nil || q.dbname == "" {
		return ""
	}
	return q.dbname
}

// SetDataBaseName 设置数据库名称
func (q *query) SetDataBaseName(dbname string) {
	q.dbname = dbname
}

// conn 操作对象
type conn struct {
	db *sql.DB
}

// SetSQLDB 设置数据库链接
func (c *conn) SetSQLDB(dbSQL *sql.DB) {
	c.db = dbSQL
}

// newTransaction 事务处理
// @param t TransactionFunc 事务处理函数
func (c *conn) newTransaction(t TransactionFunc, newTx func(tx *sql.Tx) TxSQL, option ...*sql.TxOptions) error {
	if t == nil {
		return nil
	} else if newTx == nil {
		return ErrorTx
	} else if c.db == nil {
		return DatabaseNotOpen
	} else if err := c.db.Ping(); err != nil {
		return ErrorPing
	}
	var txOption *sql.TxOptions
	if len(option) > 0 && option[0] != nil {
		txOption = option[0]
	}
	tx, err := c.db.BeginTx(context.Background(), txOption)
	if err == nil {
		defer func() {
			if err := recover(); err != nil {
				//发生异常,先回滚事务再继续抛出异常
				tx.Rollback() //回滚
				log.WithField("error", err).Panic("事务崩溃")
			}
		}()
		ntx := newTx(tx)
		if ntx == nil {
			tx.Rollback()
			return ErrorTx
		}
		e := t(ntx)
		if e != nil {
			tx.Rollback()
			return e
		}
		// 执行过程中存在错误,回滚
		if etx, ok := ntx.(TxSQLErr); ok {
			terr := etx.Err()
			if terr != nil {
				tx.Rollback()
				return terr
			}
		}
		err = tx.Commit()
		if err != nil { //事务提交失败,回滚事务,返回错误
			tx.Rollback()
		}
	}
	return err
}

// GetDb 获取数据库对象
func (c *conn) GetDb() (*sql.DB, error) {
	if c.db == nil {
		return nil, DatabaseNotOpen
	} else if err := c.db.Ping(); err != nil {
		return nil, ErrorPing
	}
	return c.db, nil
}

// SetMaxOpenConns 设置最大连接数
func (c *conn) SetMaxOpenConns(n int) {
	c.db.SetMaxOpenConns(n)
}

// SetMaxIdleConns 设置最大空闲连接数
func (c *conn) SetMaxIdleConns(n int) {
	c.db.SetMaxIdleConns(n)
}

// SetConnMaxIdleTime 设置最大空闲连接时间,超时后将关闭连接
func (c *conn) SetConnMaxIdleTime(n time.Duration) {
	c.db.SetConnMaxIdleTime(n)
}

// Close 关闭连接
func (c *conn) Close() {
	if c.db != nil {
		c.db.Close()
		c.db = nil
	}
}
