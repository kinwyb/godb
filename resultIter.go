//go:build go1.23

package godb

import (
	"iter"
)

func (r *QueryResult) IterIndexAndValue() iter.Seq2[int, map[string]interface{}] {
	return func(yield func(int, map[string]interface{}) bool) {
		if len(r.data) < 1 {
			return
		}
		for index, v := range r.data {
			dMap := map[string]interface{}{}
			for i, vv := range r.columns {
				dMap[vv] = v[i]
			}
			if !yield(index, dMap) {
				return
			}
		}
	}
}

func (r *QueryResult) Iter() iter.Seq[map[string]interface{}] {
	return func(yield func(map[string]interface{}) bool) {
		if len(r.data) < 1 {
			return
		}
		for _, v := range r.data {
			dMap := map[string]interface{}{}
			for i, vv := range r.columns {
				dMap[vv] = v[i]
			}
			if !yield(dMap) {
				return
			}
		}
	}
}
