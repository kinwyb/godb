package godb

import (
	"errors"
	"fmt"
	"github.com/xwb1989/sqlparser"
	"regexp"
	"strings"
	"testing"
)

func Test_SqlParser(t *testing.T) {
	regx := regexp.MustCompile("\\s+(\\[?[a-zA-Z0-9]+\\]+\\.)?\\[?[a-zA-Z0-9]+\\]?\\.[a-zA-Z]*\\.+")
	sql1 := "SELECT * FROM [BI].[transitDB].dbo.a INNER JOIN transitDB.dbo.b on a.x = b.x INNER JOIN [transitDB].dbo.c on c.x = b.x WHERE 1=1 "
	mh := regx.MatchString(sql1)
	dMap := map[string]string{}
	sql1 = regx.ReplaceAllStringFunc(sql1, func(s string) string {
		t.Log(s)
		news := strings.ReplaceAll(s, "[", "@")
		news = strings.ReplaceAll(news, "]", "@")
		news = strings.ReplaceAll(news, ".", "@")
		dMap[strings.TrimSpace(news)] = strings.TrimSpace(s)
		return news
	})
	t.Log(mh)
	sql2 := "SELECT * FROM a INNER JOIN b on a.x = b.x WHERE 1=1 "
	mh = regx.MatchString(sql2)
	t.Log(mh)
	s1, err1 := mysqlSelectSQLParser(sql1)
	for k, v := range dMap {
		s1.table = strings.ReplaceAll(s1.table, k, v)
	}
	if err1 != nil {
		t.Fatal(err1)
	}
	t.Log(s1)
	return

	sql := "SELECT * FROM aps_bom"
	s, err := mysqlSelectSQLParser(sql)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Printf("%v\n", s)
	sql = "SELECT * FROM aps_kint_wt A INNER JOIN aps_kint_qty_detail B ON A.`code` = B.`code` LIMIT 10,2"
	s, err = mysqlSelectSQLParser(sql)
	if err != nil {
		t.Fatal(err)
	}
	//fmt.Printf("%v\n", s)
	sql = "SELECT * FROM aps_kint_wt A INNER JOIN aps_kint_qty_detail B ON A.`code` = B.`code` WHERE A.code = ? AND A.Code IN (?,?) OR (A.Size = ? AND A.Color = ?) AND A.Code LIKE ? AND A.Code NOT LIKE ? LIMIT 10,2"
	s, err = mysqlSelectSQLParser(sql)
	if err != nil {
		t.Fatal(err)
	}
	//fmt.Printf("%v\n", s)
	sql = "SELECT * FROM aps_kint_wt A INNER JOIN aps_kint_qty_detail B ON A.`code` = B.`code` WHERE A.code = ? AND A.Code IN (?,?) OR (A.Size = ? AND A.Color = ?) AND A.Code LIKE ? AND A.Code NOT LIKE ? GROUP BY A.Code,B.Code LIMIT 10,2"
	s, err = mysqlSelectSQLParser(sql)
	if err != nil {
		t.Fatal(err)
	}
	//fmt.Printf("%v\n", s)
	sql = "SELECT * FROM aps_kint_wt A INNER JOIN aps_kint_qty_detail B ON A.`code` = B.`code` WHERE A.code = ? AND A.Code IN (?,?) OR (A.Size = ? AND A.Color = ?) AND A.Code LIKE ? AND A.Code NOT LIKE ? GROUP BY A.Code,B.Code Having Count(1) > 2 LIMIT 10,2"
	s, err = mysqlSelectSQLParser(sql)
	if err != nil {
		t.Fatal(err)
	}
	//fmt.Printf("%v\n", s)
	sql = "SELECT * FROM aps_kint_wt A LEFT JOIN (SELECT Code FROM ApsNN WHERE MMM=? GROUP BY ddd ) C ON A.`code` = B.`code` WHERE A.code = ? AND A.Code IN (?,?) OR (A.Size = ? AND A.Color = ?) AND A.Code LIKE ? AND A.Code NOT LIKE ? LIMIT 10,2"
	s, err = mysqlSelectSQLParser(sql)
	if err != nil {
		t.Fatal(err)
	}
	//fmt.Printf("%v\n", s)
	stmt, err := sqlparser.Parse(sql)
	if err != nil {
		t.Fatal(err)
	}
	switch stmt := stmt.(type) {
	case *sqlparser.Select:
		stmt.SetLimit(&sqlparser.Limit{
			Offset:   nil,
			Rowcount: sqlparser.NewIntVal([]byte("1")),
		})
	default:
		t.Fatal(errors.New("只支持select语句"))
	}
	buf := sqlparser.NewTrackedBuffer(nil)
	stmt.Format(buf)
	fmt.Printf("%s\n", sql)
	sql = buf.String()
	sql = regexp.MustCompile("(\\:v\\d+)").ReplaceAllString(sql, "?")
	fmt.Printf("%s\n", sql)
}
