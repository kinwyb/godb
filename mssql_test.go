package godb

import (
	"context"
	"errors"
	"fmt"
	"github.com/spf13/cast"
	"testing"
)

func TestMSSQLConnect(t *testing.T) {
	conn, err := MSSQLConnect("192.168.9.8", "sa",
		"bj123456", "BDDMS")
	if err != nil {
		t.Fatal(err)
	}
	defer conn.Close()
	//retmsg := &sqlexp.ReturnMessage{}
	retmsg := &SQLPrinter{}

	ctx := context.Background()
	res := conn.QueryRowsWithContext(ctx, "PRINT N'This is a message';SELECT TOP 10 * FROM Employee WHERE EmployeeNo = @p1 ;PRINT N'This is a message';PRINT N'This is a message';", retmsg.ReturnMessage(), "007928")
	if res.Error() != nil {
		t.Fatal(res.Error())
	}
	retmsg.Parse()
	fmt.Printf("%v\n", retmsg.Message)
	fmt.Printf("%v\n", retmsg.ErrMessage)
	return
	//active := true
	//for active {
	//	msg := retmsg.Message(ctx)
	//	select {
	//	case <-ctx.Done():
	//		break
	//	default:
	//		switch m := msg.(type) {
	//		case sqlexp.MsgNotice:
	//			fmt.Println(m.Message)
	//		case sqlexp.MsgError:
	//			fmt.Fprintln(os.Stderr, m.Error)
	//		case sqlexp.MsgRowsAffected:
	//			fmt.Println("Rows affected:", m.Count)
	//		case sqlexp.MsgNextResultSet:
	//			fmt.Println("结束")
	//			active = false
	//		default:
	//			fmt.Printf("%v\n", m)
	//			//active = false
	//		}
	//	}
	//}
	return
	mo := res.Get("MO")
	if cast.ToString(mo) != "CJPG20191220000231" {
		t.Fatal("查询错误1")
	}
	res = conn.QueryRow("SELECT * FROM VW_MO WHERE MO = ? ",
		"CJPG20191220000231")
	if res.Error() != nil {
		t.Fatal(res.Error())
	}
	mo = res.Get("MO")
	if cast.ToString(mo) != "CJPG20191220000231" {
		t.Fatal("查询错误2")
	}
	pg := &PageObj{}
	res = conn.QueryWithPage("SELECT * FROM VW_MO WHERE MO = ? ORDER BY MO", pg, "CJPG20191220000231")
	if res.Error() != nil {
		t.Fatal(res.Error())
	}
	mo = res.Get("MO")
	if cast.ToString(mo) != "CJPG20191220000231" {
		t.Fatal("查询错误3")
	} else if pg.Total != 1 {
		t.Fatal("分页总数错误4")
	}
	// 删除
	err = conn.Exec("DELETE FROM VW_MO WHERE MO IN (?,?)", "T123456", "T1234567").Error(false)
	if err != nil {
		t.Fatal(err)
	}
	var args = []interface{}{
		"T123456", "WT07", "SF12", "318924", "中", "", "", "2021-03-13", "", "9", "2021-03-13 17:00:01",
	}
	// commit
	conn.Transaction(func(tx TxSQL) error {
		sql := "INSERT INTO VW_MO(MO,SoNo,PONO,StyleNo,StyleDesc,Brand,CustName,TakeDate,CustStyle,Memo,LastUpdate) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
		err := tx.Exec(sql, args...).Error(true)
		if err != nil {
			fmt.Println("insert 错误:", err.Error())
			return err
		}
		args[0] = "T1234567"
		err = tx.Exec(sql, args...).Error(true)
		if err != nil {
			fmt.Println("insert 错误:", err.Error())
			return err
		}
		return nil
	})
	// rollback
	conn.Transaction(func(tx TxSQL) error {
		args[0] = "T1234568"
		sql := "INSERT INTO VW_MO(MO,SoNo,PONO,StyleNo,StyleDesc,Brand,CustName,TakeDate,CustStyle,Memo,LastUpdate) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
		err := tx.Exec(sql, args...).Error(true)
		if err != nil {
			fmt.Println("insert 错误:", err.Error())
		}
		args[0] = "T1234569"
		err = tx.Exec(sql, args...).Error(true)
		if err != nil {
			fmt.Println("insert 错误:", err.Error())
		}
		return errors.New("rollback")
	})
	mo = conn.QueryRow("SELECT * FROM VW_MO WHERE MO = ?", "T123456").Get("MO")
	if cast.ToString(mo) != "T123456" {
		t.Fatal("insert 事务错误")
	}
	// 删除
	err = conn.Exec("DELETE FROM VW_MO WHERE MO = ?", "T123456").Error(true)
	if err != nil {
		t.Fatal(err)
	}
	err = conn.Exec("DELETE FROM VW_MO WHERE MO = ?", "T1234567").Error(true)
	if err != nil {
		t.Fatal(err)
	}
	// 删除
	err = conn.Exec("DELETE FROM VW_MO WHERE MO = ?", "T1234568").Error(true)
	if err == nil {
		t.Fatal("空行删除无法检测")
	}
}

func TestMssqlTx_GetTx(t *testing.T) {
	conn, err := MSSQLConnect("192.168.9.8", "sa",
		"bj123456", "dbNautilus")
	if err != nil {
		t.Fatal(err)
	}
	//sql := "SELECT * FROM [DispatchMaster] WHERE DispatchMasterID = ''"
	//err = conn.QueryRows(sql).Error()
	//if err != nil {
	//	t.Fatal(err)
	//}
	err = conn.Transaction(func(tx TxSQL) error {
		sql := "SELECT * FROM [DispatchMaster] WHERE DispatchMasterID = ''"
		_ = tx.QueryRows(sql).Error()
		//if err != nil {
		//	return err
		//}
		sql = "INSERT INTO MACHINES_STATE (state,display,[desc]) VALUES (?,?,?)"
		return tx.Exec(sql, 4, "test", "test").Error()
	})
	if err != nil {
		t.Fatal(err)
	}
}
