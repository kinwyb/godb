package godb

import (
	"context"
	"github.com/golang-sql/sqlexp"
	"sync"
)

// SQLPrinter sql服务输出的消息,MSSQL支持
// 使用方法:
//
//		printer := &SQLPrinter{}
//		res := conn.QueryRows(sql,args1,arg2,printer.ReturnMessage())
//	 	printer.Parse()
//		fmt.Printf("Print: %v\n",printer.Message)
//		fmt.Printf("Error: %s\n",printer.ErrMessage)
//		fmt.Printf("Rows: %d\n,res.Length())
type SQLPrinter struct {
	argIndex     int      `description:"参数位置"`
	Message      []string `description:"输出的内容"`
	ErrMessage   string   `description:"输出的错误"`
	queryMessage *sqlexp.ReturnMessage
	lock         sync.Mutex
}

func (s *SQLPrinter) ReturnMessage() *sqlexp.ReturnMessage {
	s.queryMessage = &sqlexp.ReturnMessage{}
	return s.queryMessage
}

// Parse 解析消息
func (s *SQLPrinter) Parse() {
	s.lock.Lock()
	defer s.lock.Unlock()
	if s.queryMessage != nil {
		ctx := context.Background()
		active := true
		for active {
			msg := s.queryMessage.Message(ctx)
			select {
			case <-ctx.Done():
				break
			default:
				switch m := msg.(type) {
				case sqlexp.MsgNotice:
					s.Message = append(s.Message, m.Message.String())
				case sqlexp.MsgError:
					s.ErrMessage = m.Error.Error()
				case sqlexp.MsgNextResultSet:
					active = false
				}
			}
		}
	}
	s.queryMessage = nil
}
