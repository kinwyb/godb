package godb

import (
	"errors"
)

var (
	DatabaseNotOpen     = errors.New("数据库连接尚未初始化")
	DatabaseConnectFail = errors.New("数据库连接失败")
	SQLError            = errors.New("数据库操作异常")
	SQLEmptyChange      = errors.New("数据无变化")
	ErrorPing           = errors.New("数据库网络异常")
	ErrorTx             = errors.New("无法创建事务")
)
